var Bicicleta = require('../../models/bicicleta');

//SE CORRE ANTES DE CADA DESCRIBE
beforeEach(() => {
    Bicicleta.allBicis = [];
});

describe("Bicicleta.allBicis", function() {

    it("comienza vacia", function() {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
}); 

describe("Bicicleta.add", function() {
    it("agregamos una", function() {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [9.991823, -84.086227]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);

    });
});

describe("Bicicleta.findById", function(){
    it("debe encontrar una con el id", function(){
        expect(Bicicleta.allBicis.length).toBe(0);

        var aBici = new Bicicleta(1, 'rojo', 'urbana', [9.991823, -84.086227]);
        var bBici = new Bicicleta(2, 'rojo', 'urbana', [9.991823, -84.086227]);
        Bicicleta.add(aBici);
        Bicicleta.add(bBici);

        var targetBici = Bicicleta.findById(2);
        expect(targetBici.id).toBe(2);
    });
});

describe("Bicicleta.removeById", function(){
    it("debe borrar una con el id", function(){
        expect(Bicicleta.allBicis.length).toBe(0);

        var aBici = new Bicicleta(1, 'rojo', 'urbana', [9.991823, -84.086227]);

        Bicicleta.add(aBici);
        expect(Bicicleta.allBicis.length).toBe(1);
        Bicicleta.removeById(1);

        expect(Bicicleta.allBicis.length).toBe(0);
    });
});