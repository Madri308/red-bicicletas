var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www'); //Se ejecuta www y se levanta el server


//SE CORRE ANTES DE CADA DESCRIBE
beforeEach(() => {
    Bicicleta.allBicis = [];
});

describe('Bicicleta API',function(){
    describe('GET BICICLETAS /', function(){
        it('Status 200',function(){
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'rojo', 'urbana', [9.991823, -84.086227]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function(error,response,body){
                expect(response.statusCode).toBe(200);
            });
        });
    });
});

describe('POST BICICLETAS /create',() => {
    it('Status 200', (done) => {
        var header = { 'content-type' : 'application/json' };
        var a = '{"id": 10 , "color": "rojo" , "modelo": "urbana" , "lat": 9.991823 , "lng": -84.086227}';
        request.post({
            headers: header,
            url: 'http://localhost:3000/api/bicicletas/create',
            body: a
          },function(error,response,body){
            expect(response.statusCode).toBe(200);
            expect(Bicicleta.findById(10).color).toBe("rojo");
            done();
        });
    });
});

describe('UPDATE BICICLETAS /update',() => {
    it('Status 200', (done) => {
        
        var aBici = new Bicicleta(1, 'rojo', 'urbana', [9.991823, -84.086227]);
        Bicicleta.add(aBici);
        
        var header = { 'content-type' : 'application/json' };
        var a = '{"idViejo": 1 ,"id": 10 , "color": "rojo" , "modelo": "urbana" , "lat": 9.991823 , "lng": -84.086227}';
        request.put({
            headers: header,
            url: 'http://localhost:3000/api/bicicletas/update',
            body: a
          },function(error,response,body){
            expect(response.statusCode).toBe(200);
            expect(Bicicleta.findById(10).color).toBe("rojo");
            done();
        });
    });
});

describe('DELETE BICICLETAS /delete',() => {
    it('Status 200', (done) => {
        
        var aBici = new Bicicleta(1, 'rojo', 'urbana', [9.991823, -84.086227]);
        Bicicleta.add(aBici);
        
        var header = { 'content-type' : 'application/json' };
        var a = '{"id": 1}';

        request.delete({
            headers: header,
            url: 'http://localhost:3000/api/bicicletas/delete',
            body: a
          },function(error,response,body){
            expect(response.statusCode).toBe(204);
            expect(Bicicleta.allBicis.length).toBe(0);
            done();
        });
    });
});