var Bicicleta = function (id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
};

Bicicleta.prototype.toString = function (){
    return 'id: ' + this.id + '| color: ' + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function (aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if(aBici){
        return aBici;
    }else{
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
    }
}

Bicicleta.removeById = function(aBiciId){
    for(var bici = 0 ; bici < Bicicleta.allBicis.length ; bici++){
        if(Bicicleta.allBicis[bici].id == aBiciId){
            Bicicleta.allBicis.splice(bici,1);
            break;
        }
    }
}

// var a = new Bicicleta(1, 'rojo', 'urbana', [9.991823, -84.086227]);
// var b = new Bicicleta(1, 'negro', 'urbana', [9.995037, -84.085455]);

// Bicicleta.add(a);
// Bicicleta.add(b);

module.exports = Bicicleta; //SIEMRPRE