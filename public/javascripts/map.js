var map = L.map('main_map').setView([9.991823, -84.086227], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([9.991823, -84.086227]).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        console.log(result.bicicleta[0]);

        result.bicicleta.forEach(function(bici){
            L.marker(bici.ubicacion,{title: bici.id}).addTo(map);
        });
    }
})